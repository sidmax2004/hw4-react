import React from 'react';
import { Routes ,Route } from 'react-router-dom';
import Home from "../Home";
import Favourites from "../Favourites";
import Cart from "../Cart";

export default function Main() {

    return (
        <main>
            <Routes>
                <Route exact path='/' element={<Home />}/>
                <Route path='/favourites' element={<Favourites />}/>
                <Route path='/cart' element={<Cart />}/>
            </Routes>

        </main>
    );

}